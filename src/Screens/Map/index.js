import { StyleSheet, Text, View, ActivityIndicator } from 'react-native'
import React, {useState, useEffect} from 'react'
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps'
import Geolocation from '@react-native-community/geolocation'

const initialState = {
  latitude: null,
  longitude: null,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
}
const Map = () => {
  const [currentPosition, setCurrentPosition] = useState(initialState); 

  useEffect(()=>{
    Geolocation.getCurrentPosition(position => {
      const {longitude, latitude} = position.coords;
      setCurrentPosition({
        ...currentPosition,
        latitude,
        longitude
      }, [])
    }, 
      error => alert(error.message), 
      {timeout: 20000, maximumAge: 1000}
    )
    
  })

  return currentPosition.latitude ? (
    <MapView
      provider={PROVIDER_GOOGLE}
      style={{flex: 1}}
      showsUserLocation      
      initialRegion={currentPosition}
    >
      <Marker coordinate={currentPosition}></Marker>
    </MapView>
  ) : <ActivityIndicator style={{flex: 1}} animating size="large"/>
}

export default Map

const styles = StyleSheet.create({
  wrapper: {
    ...StyleSheet.absoluteFillObject
  }
})