import { View, Text, Button } from 'react-native'
import React, {useEffect} from 'react'
import crashlytics from '@react-native-firebase/crashlytics';
import messaging from '@react-native-firebase/messaging'
import * as navigation from '../../Router/rootNavigation';

async function onSignIn(user) {
  crashlytics().log('User signed in.');
  await Promise.all([
    crashlytics().setUserId(user.uid),
    crashlytics().setAttribute('credits', String(user.credits)),
    crashlytics().setAttributes({
      role: 'admin',
      followers: '13',
      email: user.email,
      username: user.username,
    }),
  ]);
}

const FirebaseScreen = () => {  
  useEffect(() => {
    crashlytics().log('App mounted.');
  }, []);

  const getToken = async() => {
    const token = await messaging().getToken()
    console.log(JSON.stringify(token))
  }
  
  useEffect(()=>{  
    getToken()
  }, [])
  
  return (
    <View>
      <Button
        title="Sign In"
        onPress={() =>
          onSignIn({
            uid: 'Aa0Bb1Cc2Dd3Ee4Ff5Gg6Hh7Ii8Jj9',
            username: 'Joaquin Phoenix',
            email: 'phoenix@example.com',
            credits: 42,
          })
        }
      />
      <Button title="Test Crash" onPress={() => crashlytics().crash()} />      
    </View>
  );
}

export default FirebaseScreen