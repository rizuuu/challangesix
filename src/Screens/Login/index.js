import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native'
import React, {useState, useEffect} from 'react'
import auth from '@react-native-firebase/auth';
import { SafeAreaView } from 'react-native-safe-area-context';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import Map from '../Map';
import * as navigation from '../../Router/rootNavigation';
// var LocalAuth = require('react-native-local-auth');


const Login = () => {
    GoogleSignin.configure({
        webClientId:
          '772768448340-vkag5t8d22arqm6hognbvvfps7lqd4b0.apps.googleusercontent.com',
      });
      const [email, setemail] = useState('');
      const [password, setpassword] = useState('');

      const onChangeEmail = input => {
        setemail(input);
      };

      const onChangePassword = input => {
        setpassword(input);
      };

      async function onGoogleButtonPress() {
        // Get the users ID token
        const {idToken} = await GoogleSignin.signIn();
    
        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    
        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
        
      }
    //   function pressHandler() {
    //     LocalAuth.authenticate({
    //       reason: 'Fingerprint Scan',
    //       fallbackToPasscode: true, // fallback to passcode on cancel
    //       suppressEnterPassword: true, // disallow Enter Password fallback
    //     })
    //       .then(success => {
    //         return navigation.navigate('botTab');
    //       })
    //       .catch(error => {
    //         Alert.alert('Scan Failed', error.message);
    //       });
    //   }
    
    
    return (
        <SafeAreaView style={styles.viewIndex}>       
            <View>
                <Text style={{fontWeight: 'bold', fontSize: 20}}>Login App</Text>
            </View> 
            <View>
            <TextInput 
                style={styles.inputText}
                placeholder={'Email'}
                onChangeText={userEmail => setemail(userEmail)}                
            />
            <TextInput 
                secureTextEntry={true}
                style={styles.inputText} 
                placeholder={'Password'}
                onChangeText={userPassword => setpassword(userPassword)}                
            />        
            </View>
            <TouchableOpacity style={styles.btnSubmit} >
                <Text style={{color: 'white', fontSize: 17, paddingTop: 5, fontWeight: 'bold'}}> Submit </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnSubmitGmail} 
                onPress={() =>
                onGoogleButtonPress().then(() => navigation.navigate('MainApp'))
            }>
                <Text style={{color: 'white', fontSize: 17, paddingTop: 5, fontWeight: 'bold'}}> Login With Gmail </Text>
            </TouchableOpacity>

            {/* <TouchableOpacity style={styles.btnSubmit} onPress={()=> pressHandler()}>
                <Text style={{color: 'white', fontSize: 17, paddingTop: 5, fontWeight: 'bold'}}> Fingerprint </Text>
            </TouchableOpacity>                    */}
        </SafeAreaView>
  )
}

export default Login

const styles = StyleSheet.create({
    viewIndex: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center', 
        alignItems: 'center'
    },
    inputText:{        
        height: 40,
        margin: 12,
        borderWidth: 1,
        width: 280,
        padding: 10,
        borderRadius: 8
    },
    btnSubmit: {        
        width: 280,
        height: 40,
        marginVertical: 10,
        marginLeft: 10,
        marginRight: 20,
        paddingVertical: 2,
        borderRadius: 8,
        textAlign: 'center',        
        alignItems: "center",
        backgroundColor: "#2196F3",                
    },
    btnSubmitGmail: {        
        width: 280,
        height: 40,
        marginVertical: 10,
        marginLeft: 10,
        marginRight: 20,
        paddingVertical: 2,
        borderRadius: 8,
        textAlign: 'center',        
        alignItems: "center",
        backgroundColor: "#ef594e",                
    }
})