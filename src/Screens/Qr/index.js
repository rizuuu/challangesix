import { StyleSheet, Text, View, Alert } from 'react-native'
import React, {useState} from 'react'
import {CameraScreen, Camera, CameraType} from 'react-native-camera-kit';
import { useIsFocused } from '@react-navigation/native';



const QrCode = () => {
  // const isFocused = useIsFocused();
  // const onReadCode = (data) => {
  //   alert(data.nativeEvent.codeStringValue)
  // }
  return (
    <View style={{height: 750}}>
      <CameraScreen  
      // Barcode props
      CameraType={CameraType.Back}
      scanBarcode={true}
      onReadCode={(event) => Alert.alert(`url : ${event.nativeEvent.codeStringValue}`)} // optional
      showFrame={true} // (default false) optional, show frame with transparent layer (qr code or barcode will be read on this area ONLY), start animation for scanner,that stoped when find any code. Frame always at center of the screen
      laserColor='red' // (default red) optional, color of laser in scanner frame
      frameColor='white' // (default white) optional, color of border of scanner frame
      />
    </View>
  )
}

export default QrCode

const styles = StyleSheet.create({})