import { StyleSheet, Text, View, ScrollView, RefreshControl } from 'react-native'
import React, {useState, useCallback} from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Map from '../Screens/Map';
import QrCode from '../Screens/Qr';
import FirebaseScreen from '../Screens/Home';
import Login from '../Screens/Login';
import * as navigation from '../Router/rootNavigation';



const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const mapName = 'Map';
const QrCodeName = 'QrCode';
const firebaseName = 'FirebaseScreen';
  
const Router  = () => {    
    return (
        <Stack.Navigator initialRouteName='MainApp'>
            <Stack.Screen name = "MainApp" component={BotTab} options={{headerShown: false}}/>            
            <Stack.Screen name = "Login" component={Login} options={{headerShown: false}}/>
        </Stack.Navigator>
  )
}

export default Router 

const styles = StyleSheet.create({})

export function BotTab(){ 
    return(
        <Tab.Navigator            
            initialRouteName={firebaseName}
            screenOptions = {({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                let iconName;
                let rn = route.name;

                if (rn === mapName){
                    iconName = focused ? 'map' : 'map-outline';
                } else if (rn === QrCodeName){
                    iconName = focused ? 'qr-code' : 'qr-code-outline';
                } else if (rn === firebaseName){
                    iconName = focused ? 'logo-firebase' : 'logo-firebase';
                }

                return <Ionicons name={iconName} size={size} color={color}/>;                    
            },        
            tabBarStyle: {height: 60, paddingBottom: 12},        
            })}      
        >

        <Tab.Screen name={mapName} component={Map} options={{ headerShown: false }}/>
        <Tab.Screen name={QrCodeName} component={QrCode} options={{ headerShown: false }}/>
        <Tab.Screen name={firebaseName} component={FirebaseScreen} options={{ headerShown: false }}/>
    </Tab.Navigator>
    )
}